Vim YouCompleteMe for Windows
=============================

--------------------------------------------------------------------------------

    :::text
                                          ▒██▓
                                         ██████▓
          ▓███████████████████████████████▒▒▓▒███░  ▓█████████████████████████████
         ███                           ██▓▓▓▓▓▒▒██▓███                          ███
         ██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒███▓▓▓▓█▓▒▒███▒ ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓██░
         ██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒░▒▓███▓▓▓▓▓▓█▓▒██▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓██░
         ██▓ ▒▓▓▓ ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓▓▓▓███▓▓▓▓▓▓▓▓▓██▒ ▒▒▒▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓███░
         ▓██████  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓██████▓▓▓▓▓▓▓▓▓▓███▓███▒▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓████
          ░█████  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓█████▓▓▓▓▓▓▓▓▓▓▓▓▓████▒ ▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓████▒
             ▒██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓██▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓███▒ ▒▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓███▓
             ▒██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓██▓▓▓▓▓▓▓▓▓▓▓▓▓███▓  ▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓████
             ▓██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓██▓▓▓▓▓▓▓▓▓▓▓████░ ▒▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓████▒
             ▓██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓██▓▓▓▓▓▓▓▓▓████▒ ▒▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓███▓
             ▓██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓██▓▓▓▓▓▓▓▓███▓ ░▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓███▓
             ▓██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓██▓▓▓▓▓▓███▓  ▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓██▓░
             ▓██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓██▓▓▓▓████▒ ▒▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓█████░
             ▒██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓██▓▓▓███▒ ░▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓████▓▒▓██
             ▓██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓██████▓  ▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓███▓▓▓▓▒▒██▓
           ░████  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓█████░ ▒▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓███▓▓▓▓▓▓█▓▒▓██▓
          ███▒██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓███▒ ▒▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓████▓▓▓▓▓▓▓▓▓█▓▒▓██░
        ▓██▒░▓██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓█▓  ▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓████▓▓▓▓▓▓▓▓▓▓▓▓▓▓▒▒███
      ▒██▓▒▓▓███  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓  ▒▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓███▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓█▓▒▓██▓
    ░███▒▒▓▓▓▓██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓░▒▓▒▒▒▒▒▒▒▒▒▒▒▒░▒▒▓▓████▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▒▓██▓
    ▓██▓▓█▓▓▓▓██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓██████▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓██▓███
     ▒███▓█▓▓▓██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒████▓▓██▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓█████▓
       ▓███▓▓███  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒░▓█▒ ░ ░██▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓█████▓
         ▓██▓▓██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓█▓ ░░░▓█▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓██████
          ░█████  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓███████▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓████▓░
            ▒███  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓█████████▓▓███████▓▓▓▓██████████▓▒▒▓▓▓
             ▒██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓██▓▓███▓██▓██▓██████████████████████████▒
             ▒██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓██▓▓▒░░░ ████▓▓░░░░░▓██▓▒░░░░▒▓▓▓▓░░▒▒░▒██
             ▓██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓██████▒▒▒░▓█▓▓██▓▒▒▒▒░░  ░▒▒▒▒▒░░░░░▒▒▒▒░▓█▓
             ▓██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓████▓▓█▒▒▒▒▒██▓▓██▒▒▒▒██████▒▒▒▒▒█████▓▒▒▒▒██
             ▓██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓███▓▓▓█▓░▒▒░██▓▓▓█▒░▒░▓██████▒▒▒▒██▒░██▒▒▒░▓█▓
             ▓██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓████▓▓▓██▒▒▒▒▒██▓▓██░▒▒▒██▓▓██▒▒▒░▓█▒ ░█▓░▒▒▒██
             ▓██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▓████▓▓▓▓▓█▓░▒▒░██▓▓██▒▒▒░▓██▓██▓░▒▒▒██  ██▒▒▒░▓█▒
             ▓██  ▒▒▒▒▒▒▒▒▒▒▒▓▓███▓▓▓▓▓▓██░▒▒░▓█▓▓▓█▓░▒▒▒██████▒▒▒░▓█░ ▒█▓░▒▒▒██
             ▓██  ▒▒▒▒▒▒▒▒▒▓▓████▓▓▓▓▓▓██▒░▒▒▒██████░░▒ ▓██▓░█▓ ▒░▒███ ██░▒▒ ▓███
             ▓██  ▒▒▒▒▒▒▒▒▓██████▓█▓▓▓▓██▒▒▒▒▒▒▓███▓▒▒▒▓▓██ ██▓▓▓▓▓▓█▓▒██▓▓▓▓▓██▓
             ▓██   ▒▒▒▒▒▓████▒ ▓███▓█▓███████████▓████████▓ █████████ ██████████
             ▒█████████████▓     ▓██▓▓▓▓▓▓▓▓▓▓▓▓▓▓█████░
               ▓██████████        ░███▓▓▓▓▓▓▓▓▓▓█████▓
                                    ▓███▓█▓▓▓▓█████▓
                                      ▓███▓███████
                                        ████████▒
                                         ▒████▓
                                          ▒██▓

Table of Contents
-----------------

--------------------------------------------------------------------------------

  - [Description](#markdown-header-description)
  - [Platforms](#markdown-header-platforms)
  - [Architectures](#markdown-header-architectures)
  - [Installation](#markdown-header-installation)
  - [Downloads](#markdown-header-downloads)

Description
-----------

--------------------------------------------------------------------------------

Building the native back end of the Vim plugin,
[YouCompleteMe][GitHub/Valloric/YouCompleteMe] (YCM) by [Val Markovic][], has
proven to be difficult for inexperienced users. The goal of this project is to
provide native builds of YCM for Windows for both x86 (32-bit) and x64 (64-bit)
architectures.

**NOTE:** If you are interested which toolchain I use to build YCM for Windows
as well as other native software for Windows in general, then I'd be glad to say
that I'm using [MinGW-w64][] (not [MinGW][]!) which is a production quality
toolchain, bringing bleeding-edge [GCC][Wikipedia/GCC] features to Windows for
both x86 and x64 architectures.

Platforms
---------

--------------------------------------------------------------------------------

  - Windows 2000;
  - Windows XP;
  - Windows Vista;
  - Windows 7;
  - Windows 8.

Architectures
-------------

--------------------------------------------------------------------------------

  - x86, x86-32, x32, i686;
  - x64, x86-64, amd64.

Installation
------------

--------------------------------------------------------------------------------

 1. Make sure that your current Vim version is at least 7.3.584 and that it was
    compiled with support for Python 2, in particular, 2.6 or 2.7, and if you
    don't have one, then you could opt to my [Vim for Windows][];
 2. Make sure that the directory containing Python 2 interpreter (`python.exe`)
    is in the `PATH` environment variable;
 3. Since the plugin relies on [LLVM/Clang][] to provide accurate semantic
    completion framework, you will need to install it as well, in particular,
    all you need is `libclang.dll`, and if you don't have one, then you could
    opt to my [LLVM for Windows][];
 4. Make sure that the directory containing `libclang.dll` is either in the
    `PATH` environment variable (recommended) or right next to `ycm_core.pyd`
    (in the `python` subdirectory of the plugin root directory);
 5. Go to [Downloads](#markdown-header-downloads) and obtain the archive with
    desired version and architecture that matches both your Vim and
    `libclang.dll`;
 6. Finally, extract the archive and deploy the plugin with your favorite
    method.

**NOTE:** It is very important to make sure that architectures of all 3
components (Vim, `libclang.dll`, YCM) match, and it is even more important to
truly understand why, not to make stupid mistakes. So if you don't understand
it, then you should probably go and read at least some Wikipedia.

**NOTE:** Microsoft Visual C Runtime (MSVCR) is notorious of having many
different versions, some of which are buggy or/and break backward compatibility.
If after properly installing YCM you experience the following error dialog when
starting Vim:

    :::text
    Runtime Error!

    Program: <vim-dir>\gvim.exe

    R6034
    An application has made an attempt to load the C runtime
    library incorrectly.
    Please contact the application's support team for more
    information.

then congratulations, you've just plunged into that shit. To solve that,
carefully search all the directories in the `PATH` environment variable for
`msvcr90.dll` and eliminate them (at least for Vim startup). It is good, in
general, to keep your `PATH` clean and to have there only those directories that
you currently need as it prevents situations like this. For example, it is
common these days that people have Intel iCLS Client (which is absolutely
useless by the way) installed which contains old/buggy version of `msvcr90.dll`,
and it adds itself to the system `PATH` environment variable by default. You'll
have to definitely eliminate this one. Anyway, these giant corporations will
screw us all one day...

Downloads
---------

--------------------------------------------------------------------------------

**NOTE:** Since YCM is not using any versioning, commit hashes are used instead.

  - x86:
      - **[cf62110][Downloads/Vim YCM/cf62110/Windows/x86]**;
      - [ee12530][Downloads/Vim YCM/ee12530/Windows/x86].

  - x64:
      - **[cf62110][Downloads/Vim YCM/cf62110/Windows/x64]**;
      - [ee12530][Downloads/Vim YCM/ee12530/Windows/x64].

[Wikipedia/GCC]: http://en.wikipedia.org/wiki/GNU_Compiler_Collection

[MinGW]:     http://www.mingw.org/
[MinGW-w64]: http://mingw-w64.sourceforge.net/

[LLVM/Clang]: http://clang.llvm.org/

[GitHub/Valloric/YouCompleteMe]: http://valloric.github.io/YouCompleteMe/

[Val Markovic]: http://val.markovic.io/

[Vim for Windows]:  /Haroogan/vim-for-windows/
[LLVM for Windows]: /Haroogan/llvm-for-windows/

[Downloads/Vim YCM/cf62110/Windows/x86]: /Haroogan/vim-youcompleteme-for-windows/downloads/vim-ycm-cf62110-windows-x86.zip
[Downloads/Vim YCM/cf62110/Windows/x64]: /Haroogan/vim-youcompleteme-for-windows/downloads/vim-ycm-cf62110-windows-x64.zip

[Downloads/Vim YCM/ee12530/Windows/x86]: /Haroogan/vim-youcompleteme-for-windows/downloads/vim-ycm-windows-x86.zip
[Downloads/Vim YCM/ee12530/Windows/x64]: /Haroogan/vim-youcompleteme-for-windows/downloads/vim-ycm-windows-x64.zip
